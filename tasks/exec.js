"use strict";

var cp = require("child_process");

function exitCallback(grunt, self, options, err, stdout, stderr) {
	if (self.timer) {
		clearTimeout(self.timer);
	}
	if (options.stdout) {
		grunt.log.write("stdout: " + stdout);
	}
	if (options.stderr) {
		grunt.log.write("stderr: " + stderr);
	}
	if (self.exitCode) {
		grunt.log.write("exit code: " + self.exitCode);
	}
	if (self.exitSignal) {
		grunt.log.write("exit signal: " + self.exitSignal);
	}

	var succeed = true;
	
	if (err) {
		grunt.log.error("Error executing child process: " + err.toString());
		succeed = false;
	}
	if (options.exitCodes.indexOf(self.exitCode) === -1) {
		grunt.log.error("Exit code unexpected");
		succeed = false;
	}
	
	self.done(succeed);
}

function onTimeout(grunt, self, options) {
	grunt.log.error("Execution timeout expired");
	process.kill(self.childProcess.pid, options.killSignal);
	self.done(false);
}

function onChildExit(grunt, self, code, signal) {
	self.exitCode = code;
	self.exitSignal = signal;
}

function doExec(grunt, self) {
	var options = self.options({
		command: "/bin/true",
		args: [],
		stdout: true,
		stderr: true,
		cwd: ".",
		exitCode: 0,
		exitCodes: [],
		timeout: 0,
		killSignal: "SIGTERM"
	});

	self.done = self.async();

	options.exitCodes.push(options.exitCode);

	if (!options.command) {
		grunt.log.error("Missing command property.");
		return self.done(false);
	}

	grunt.log.debug(JSON.stringify(options));

	self.childProcess = cp.execFile(options.command, options.args, function(err, stdout, stderr) {
		exitCallback(grunt, self, options, err, stdout, stderr);
	});

	if (grunt.verbose) {
		grunt.verbose.writeln("pid: " + self.childProcess.pid);
	}

	self.childProcess.on('exit', function(code, signal) {
		onChildExit(grunt, self, code, signal);
	});
	
	if (options.timeout) {
		self.timer = setTimeout(function() {
			onTimeout(grunt, self, options);
		}, options.timeout * 1000);
	}
}

function registerExecTask(grunt) {
	grunt.registerMultiTask("exec", "Execute commands", function() {
		doExec(grunt, this);
	});
	grunt.log.debug("done!");
}

module.exports = registerExecTask;
