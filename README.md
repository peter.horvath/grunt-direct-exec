# grunt-direct-exec
Async Grunt task to start any external command like a grunt task in a configurable environment

Example:

    module.exports = function(grunt) {
      'use strict';
      
      grunt.initConfig({
        
        exec : {
          deploy : {
            options : {
              command : 'rsync',
              args: ['-vaH', '--delete', './dist/', 'user@somewhere:/remote/server/path/'],
              timeout: 20
            }
          }
        }
        
      });
      grunt.loadNpmTasks("grunt-direct-exec");
    };


All configuration is inside an "options:" element in the Grunt config JSON, it is an unfortunate
syntactical limitation of the Grunt.

This example will call the command `rsync -vaH --delete ./dist/ user@somewhere:/remote/server/path/`
by calling `grunt exec:deploy`.

The valid options are the following:

* `command`: the binary (or script) path what we want to call.
* `args`: an array of string(s), it will be the arguments of the called binary.
* `timeout`: in seconds - after that, the async task will be closed an unsuccessful and the still
  running child process will be killed.
* `stdout`: boolean value. If true, the buffered standard output of the command will be printed
  in the grunt log.
* `stderr`: also boolean, the same for the standard error.
* `exitCode`: an integer. If given, the task will be considered successful if the process exited
  with the exit code given.
* `exitCodes`: an array of integers. The same as `exitCode`, but for multiple exit codes.
